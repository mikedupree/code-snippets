<?php

/**
 * AIR MILES Number Check Digit Algorithm - Modified Modulus 11  AIR MILES Card Number must be validated at point of entry/capture within the Sponsor’s systems.
 *  The formula below is a non-standard MOD11 algorithm that has been modified to meet AMMIS controls and standards.
 *  Define the first 10 digits of the Collector Number as D1-D10 (left through right) and compute the digit totals as follows:
 *  (D1*6)+(D2*5)+(D3*4)+(D4*3)+(D5*8)+(D6*7)+(D7*6)+(D8*5)+(D9*4)+(D10*3)
 *  Divide the digit total by 11 (using a 2-digit remainder)
 *  If the remainder is less than 2, the check digit equals 0
 *  If the remainder is 2 or greater, the check digit equals 11 minus the remainder.
 *  If Check Digit = D11 verification passed
 * @param $number_to_check
 *
 * @return bool
 */
function mod_11_validation($number_to_check){
  $multipliers = [6, 5, 4, 3, 8, 7, 6, 5, 4, 3];
  $number_string = (string) $number_to_check;
  $sum = 0;
  foreach($multipliers as $key => $multiplier){
    if ($key + 1 == 11) {//11 is last digit and is not multiplied
      break;
    }

    $sum += (int) $number_string[$key] * $multiplier;
  }

  $remainder = $sum % 11;

  $check_digit = 0;
  if($remainder >= 2){
    $check_digit = 11 - $remainder;
  }

  if($check_digit == $number_string[10]){
    return TRUE;
  }

  return FALSE;
}

